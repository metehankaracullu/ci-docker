ARG TERRAFORM_VERSION="1.4.4"

FROM hashicorp/terraform:${TERRAFORM_VERSION} AS terraform
FROM alpine:3.19

COPY --from=terraform /bin/terraform /usr/bin/terraform

RUN apk add --no-cache curl \
    && curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" \
    && unzip awscliv2.zip \
    && ./aws/install \
    && rm -rf awscliv2.zip ./aws

WORKDIR /opt
CMD ["/bin/sh"]
